var app = angular.module("myApp", ["ui.router", "ngResource", "ngCookies"]);
app.constant('config', {
    appDataEnv: 'mock',
    login: 'demo',
    password: 'rele',
    version: '0.24.578',
    sessionTimeout: 30,
    deviceName: 'Rele 1/0'
});
// debugger;
// app.factory('GetData', ['$resource',
//     function ($resource) {
//         return $resource(":url", {});
//     }
// ]);
app.factory('auth', ['config', '$cookies', '$state', '$resource',
    function (config, $cookies, $state, $resource) {
       
        return function ($scope) {
            //  alert('auth');
            //   alert($resource);
            return {
                "check": function () {
 
                    let sessionObj = $cookies.get('sessionObj');
                    if (sessionObj == undefined) {
                        console.log(`check false, obj undefined`);
                        return false;
                    }
                    let cookies = JSON.parse(sessionObj);
                    // console.log(`flag ${cookies.authflag}`);
                    // console.log(`expires ${new Date().getTime() - cookies.expiredAt}`);
                    if (cookies.authflag && (cookies.expiredAt - new Date().getTime() > 0)){
                    //   < (config.sessionTimeout * 1000)) {
        
                        console.log(`check true all ok`);
                        return true;
                    }
                    console.log(`check false, session expired or authflag false`);
                    return false;
                },
                "tryAuth": function (login, password) {
                    alert(`Try auth`);
                    let hashCode = (str) => {
                        let hash = 0;
                        for (let i = 0; i < str.length; i++) {
                            hash = ~~(((hash << 5) - hash) + str.charCodeAt(i));
                        }
                        hash = hash * 1000;
                        return hash.toString(36);
                    };
                    // alert(hashCode(`${login}${password}`));
                    // if (login === config.login && password === config.password) {
                    //     $cookies.put('authObj', JSON.stringify({
                    //         'flag': true,
                    //         'time': Date.now()
                    //     }));
                    let url = `/api/login`;
                    // alert(url);
                    // alert($resource);
                    $resource(":url", {});
                    let MakeLogin = $resource(url, {}, {
                        login: {
                            method: 'POST'
                        }
                    });
                    // alert(`makelogin ${MakeLogin}`);
                           // params: {
                            //     authToken: hashCode()
                            // }
                    let hash = hashCode(`${login}${password}`);
                    // alert(`HASH: ${hash}`);
        
                    MakeLogin.login({
                        authToken: hash
                    }).$promise.then(data => {
                            // alert('aaaa');
                            // alert(`login promise result: ${data}`);
                            // alert(JSON.stringify(data));
                            // data = JSON.parse(data);
                            // alert(`data`, data.status, data.result.authStatus);
                            if (data.status == `OK` && data.result.authStatus === true) {
                                $cookies.put('sessionObj', JSON.stringify({
                                    'sessionToken': data.result.sessionToken,
                                    'authflag': true,
                                    'expiredAt': data.result.expiredAt
                                }));
                                $state.go('root./');
                            } else {
                                $scope.message = "Ooops shit was happen";  
                            }
                        }, error => {
                            alert(`login promise result: ${error}`);
                            $scope.message = "Incorrect values";
                        });
                    // } else {
                    //     $scope.message = "Incorrect values";
                    // }
                },
                "logout": function () {
                    $cookies.put('sessionObj', JSON.stringify({
                        'sessionToken': '',
                        'authflag': false,
                        'expiredAt': 0
                    }));
                    //1) socketsIO disonnection|close
                    //2) session close
                }
            };
        }
    }
]);

// app.config.$inject = ['config', '$cookies'];
app.config.$inject = ['$cookies'];
app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function ($stateProvider, $urlRouterProvider, $locationProvider) {
    $urlRouterProvider.otherwise('/');
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });
    $stateProvider
        .state('root', {
            url: '',
            abstract: true,
            views: {
                'rootCont': {
                    templateUrl: './pages/pageContainer.html'
                    //controller: 'AppCtrl'
                }
            }
        })
        .state('root./', {
            url: '/',
            //  redirectTo: function(){
            //      alert(JSON.stringify($cookies));
            //          return 'root.login';
            //  },
            views: {
                'rootCont@': {
                    templateUrl: './pages/pageContainer.html',
                },
                'pageContent@': {
                    templateUrl: './pages/mainPage.html',
                    //controller: 'AppCtrl'
                }
            },
            resolve: {
                resObj: function (auth, $state) {
                    alert(`check `, auth().check());
                    if (auth().check() === false) {
                        $state.go('root.login');
                    }
                }
            }
        })
        .state('root.page', {
            url: '/{page}',
            views: {
                'pageContent@': {
                    templateUrl: function ($stateParams) {
                        let reqPage = ['main', 'poe', 'pirepheral', 'sockets', 'services', 'communications', 'settings', 'stats', 'webproxy', 'vpn', 'ups', 'log']
                            .indexOf($stateParams.page) != -1 ? $stateParams.page : 'login';
                        return "./pages/" + reqPage + "Page.html";
                    }
                },
            },
            resolve: {
                resObj: function (auth, $state) {
                    if (auth().check() === false) {
                        $state.go('root.login');
                    }
                }
            }

        })
        .state('root.login', {
            url: '/login',
            views: {
                'rootCont@': {
                    templateUrl: './pages/loginPage.html',
                    //  controller: 'AuthCtrl'
                    controller: 'AuthCtrl'
                }
            }
        })
        .state('root.logout', {
            url: '/logout',
            resolve: {
                resObj: function (auth, $state) {
                    auth().logout();
                    // socket.close();
                    $state.go('root.login');
                }
            }
        })
}]);

app.factory('GetData', ['$resource',
    function ($resource) {
        return $resource(":url", {});
    }
]);
app.factory('GetFactData', ['GetData',
    function (GetData) {

        return function (inParams, $scope, pageLocation) {

            if (inParams.local !== null) {
                data = JSON.parse(inParams.local);
                $scope.raw = JSON.parse(JSON.stringify(data));
                $scope[pageLocation] = data[pageLocation];
            } else {
                let url = inParams.remote;
                GetData.get({
                    url: url
                }, function (data) {
                    $scope[pageLocation] = JSON.parse(data['result'])[pageLocation];
                    //data[pageLocation];
                    //alert(data['result']);
                    $scope.raw = JSON.parse(JSON.stringify($scope[pageLocation]));
                }, function (error) {
                    $scope[pageLocation] = {};
                    // raw = {};
                });
            }
        }
    }
]);
app.factory('SetSwitcherClasses', function () {
    return function (inObj, paramsObj) {
        if (paramsObj.switcherFor == 'sockets' || paramsObj.switcherFor == 'poe') {
            inObj[paramsObj.switcherId].status = !inObj[paramsObj.switcherId].status;
        } else {
            paramsObj['currentStatus'][paramsObj.switcherId] = !paramsObj['currentStatus'][paramsObj.switcherId];
        }
    }
});
app.factory('StateManipulations', ['$timeout',
    function ($timeout) {
        return function ($scope, pageLocation) {
            $scope.saveStateChanges = function () {
                let delay = 0;
                $timeout(function () {
                    //data.$save();
                    raw = $scope.raw;
                    let save = JSON.parse(JSON.stringify($scope[pageLocation]));
                    raw[pageLocation] = save;
                    let itemName = `releApp${pageLocation.replace(pageLocation.charAt(0),
                        pageLocation.charAt(0).toUpperCase())}`;
                    window.localStorage.setItem(itemName, JSON.stringify(raw));
                    $scope.hiddenButtonsFlag = false;
                    $scope.hiddenSocketIndex = -1;
                    $scope.$root.modalWindowShowingFlag = false;
                    alert('Changes was successfully applied');
                }, delay);
            }
            $scope.cancelStateChanges = function () {
                raw = $scope.raw;
                $scope[pageLocation] = JSON.parse(JSON.stringify(raw[pageLocation]));
                $scope.hiddenButtonsFlag = false;
                $scope.hiddenSocketIndex = -1;
                $scope.$root.modalWindowShowingFlag = false;
            }
        }
    }
]);
app.factory('CommentStateManipulation', function () {
    return function ($scope, location) {
        return function (index) {
            let raw = $scope.raw;
            if (raw[location][index].comment != $scope[location][index].comment) {
                if ($scope.hiddenButtonsFlag == false) {
                    $scope.hiddenButtonsFlag = !$scope.hiddenButtonsFlag;
                    $scope.hiddenSocketIndex = index;
                }
            } else if ($scope.hiddenButtonsFlag == true) {
                $scope.hiddenButtonsFlag = false;
                $scope.hiddenSocketIndex = -1;
            }
        }
    }
})
app.factory('PageInit', ['$timeout', 'config', 'SetSwitcherClasses', 'GetFactData', 'StateManipulations', 'CommentStateManipulation',
    function ($timeout, config, SetSwitcherClasses, GetFactData, StateManipulations, CommentStateManipulation) {
        return function (dataPathByConfig, $scope, locationFlag) {
            /**Get Data for template from mock/server */
            GetFactData(dataPathByConfig[config.appDataEnv], $scope, locationFlag);
            /**Bind state methods manipulation */
            StateManipulations($scope, locationFlag);
            /**Comment section state|buttons manipulation */
            $scope.hiddenButtonsFlag = false;
            $scope.hiddenSocketIndex = -1;
            /**Showing modal window with button in page top */
            $scope.showModalWindow = function () {
                $scope.$root.modalWindowShowingFlag = true;
            }
            $scope.showHideButtons = CommentStateManipulation($scope, locationFlag);
            /**END of comment section state|buttons manipulation */
            /** Slider switcher Event */
            $scope.turnOnOff = function (inObj, inParamsObj) {
                SetSwitcherClasses(inObj, inParamsObj);
                $scope.$root.modalWindowShowingFlag = true;
            }
            /** END of Slider switcher Event */
            /***Modal Window Buttons**/
            $scope.$root.applyChanges = $scope.saveStateChanges;
            $scope.$root.testChanges = function () {
                alert('test already dont work yet, so all changes will be cancled');
                //$scope.$root.modalWindowShowingFlag = false;
                $scope.cancelStateChanges();
            }
            $scope.$root.cancelChanges = $scope.cancelStateChanges;
            /** END of Modal windows buttons */
        }
    }
]);

app.factory('socket', [function ($scope) {
    //    var socket = io('http://192.168.100.101:3000');//io.connect();
    //   console.log(socket);
    // return function(){ alert('aaaaa')};
    // return function(){ alert(socket);}
    return {
        on: function (eventName, callback) {
            socket.on(eventName, function () {
                var args = arguments;
                $scope.$root.$apply(function () {
                    callback.apply(socket, args);
                });
            });
        },
        emit: function (eventName, data) { //, callback) {
            // alert(eventName);
            //    alert(data);
            socket.emit(eventName, data); //);
            //   , function () {
            //     var args = arguments;
            //     $scope.$root.$apply(function () {
            //       if (callback) {
            //         callback.apply(socket, args);
            //       }
            //     });
            //   })
        }
    };
}]);