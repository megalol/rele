
function mobileBandsCategory(typeOfTech){  

    var tech = ['gsm', 'cdma', 'umtsFDD', 'umtsTDD', 'lteFDD', 'lteTDD'];

     for(var i = 0; i < 6; i++){
     
          if(typeOfTech == 'auto' || typeOfTech != tech[i]){  
              
               document.getElementById(tech[i]+'Bands').style.display = 'none';
          
          } else {
              
              document.getElementById(typeOfTech+'Bands').style.display = '';
        }  
     }
}




function dropDownOrHideTable(divID, legend){
    
    var divObject = document.getElementById(divID); 
     var divObjectLegend = document.getElementById(divID+"Legend"); 

    if(divObject.style.display != 'none'){

        divObject.style.display = 'none';
        divObjectLegend.innerHTML = '[+]&nbsp&nbsp'+legend;

      } else { 		
      
         divObject.style.display = '';
          divObjectLegend.innerHTML = '[-]&nbsp&nbsp'+legend;
    }  
}		

function AuthTypeIPType(technology, type){

     var AuthTypeV4 = document.getElementsByName(technology+'AuthTypeV4');
     var AuthTypeV6 = document.getElementsByName(technology+'AuthTypeV6');
     
        var v4Types = ['IPoE_DynamicV4', 'IPoE_StaticV4', 'IPoE_PPoEV4', 'IPoE_PPTPV6'];
        var v6Types = ['IPoE_DynamicV6', 'IPoE_StaticV6', 'IPoE_SLAACV6'];
        
        var mainTypes = ['EthernetIPTypeV4', 'EthernetIPTypeV6', 'EthernetIPTypeDualStack', 'MobileIPTypeV4', 'MobileIPTypeV6', 'MobileIPTypeDualStack'];
        
    
}

function changeCommunicationSubPage(currentID, currentClassName){
 
   var disabledTable; var enabledTable;
   
    if(currentClassName != 'activeCommunicationPageButton'){

        switch(currentID){
  
         case 'MobilePageSettings':
           
              disabledTable = 'Ethernet';
           enabledTable = 'Mobile';
          break;
          
         case 'EthernetPageSettings': 
          disabledTable = 'Mobile';
           enabledTable = 'Ethernet';
          break;
        }  
        
        document.getElementById(disabledTable+'ConfigsPage').style.display = 'none';
        document.getElementById(enabledTable+'ConfigsPage').style.display = '';
        
        document.getElementById(disabledTable+'PageSettings').className = 'passiveCommunicationPageButton';
        document.getElementById(currentID).className = 'activeCommunicationPageButton'; 	
    }			 
}