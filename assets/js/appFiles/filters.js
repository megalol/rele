app.filter('boolToText', function () {
  return function (input) {
    [obj] = input;
    if ((obj.page == 'poe' || obj.page == 'sockets') && obj.statusType == 'text') {
      return obj.status ? 'Plugged' : 'Unplagged';
    }
    if ((obj.page == 'main') && obj.statusType == 'text') {
      return obj.status ? 'Enabled' : 'Disabled';
    }
    if ((obj.page == 'sockets') && obj.statusType == 'textOnOff') {
      return obj.status ? 'ON' : 'OFF';
    }
  }
});
app.filter('setSocketsRightImageByStatus', function () {
  return function (inArr) {
    let imgUrl = (colorStatus) => `./assets/img/c13_socket${colorStatus}.png`;
    [obj] = inArr;
    let Objstatus = {
      true: {
        true: "Green",
        false: 'Red'
      },
      false: {
        true: 'Orange',
        false: 'Black'
      }
    };
    return imgUrl(Objstatus[obj.status][obj.cableStatus]);
  }
});
app.filter('mainPageMobileSignalLevel', function () {
  return function (inValue) {
    let valToWord = {
      [!inValue ? true : false]: 'No signal',
      [inValue > 0 && inValue <= 15 ? true : false]: 'Very bad',
      [inValue > 15 && inValue < 30 ? true : false]: 'Bad',
      [inValue >= 30 && inValue < 50 ? true : false]: 'Good',
      [inValue >= 50 ? true : false]: 'Excellent'
    };
    return inValue == 0 ? valToWord.true : `${valToWord.true} (-${100-inValue} dBm)`;
  }
});