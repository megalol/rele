app.controller("AppCtrl", ['$scope', '$interval', 'GetData', 'config', 'auth', 'socket',
    function ($scope, $interval, GetData, config, auth, socket) {
        // let url = "assets/js/mock/deviceMonitor.json";
        var socketio = io('https://192.168.100.101');
        let url = 'api/sysmon';
        // alert(JSON.stringify(socket()));
        // alert(socket);
        //alert(JSON.stringify(app.$config));
        GetData.get({
            url: url
        }, function (data) {
            $scope.$root.monitor = data.result; //JSON.parse(data).result;//data;
            $scope.locationFlag = '/';
            $scope.$root.modalWindowShowingFlag = false;
            let refreshMonitor = function () {
                // $scope.monitor.cpuLoad = Math.floor(Math.random() * 101);
                // $scope.monitor.memoryUsage = Math.floor(Math.random() * 6) + 20;
                // // $scope.$root.monitor.signalLevel = Math.floor(Math.random() * 101);
                // $scope.monitor.signalLevel = Math.floor(Math.random() * 101);
                // window.location = 'login';
                if (auth().check() !== false) {
                    // GetData.get({
                    //     url: url
                    // }, function (data) {
                //   alert(socket.emit);
                //    alert(
                       socketio.emit('/api/sysmon', {
                        'method': 'GET',
                        'path': 'sysmon'
                    });
                    socketio.on('/api/sysmon', (sdata) => {
                        // alert(data);
                        sdata = JSON.parse(sdata);
                        // alert(sdata);
                        $scope.monitor.cpuLoad = sdata.result['DeviceCPU'];
                        $scope.monitor.memoryUsage = sdata.result['DeviceMemory'];
                        $scope.monitor.signalLevel = sdata.result['DeviceCommunications']['wireless']['ports']['ath0']['signalLevel'];
                    });
                    // ());

                    // $scope.monitor.cpuLoad = data.result['DeviceCPU'];
                    // $scope.monitor.memoryUsage = data.result['DeviceMemory'];
                    // $scope.monitor.signalLevel = data.result['DeviceCommunications']['wireless']['ports']['ath0']['signalLevel'];
                    // });
                } else {
                    window.location = 'login';
                    $interval.cancel($scope.$root.timers.monitor);
                    // socketio.close();
                    socketio.disconnect();
                }
            }

            if ($scope.$root.intflag === undefined) {

                refreshMonitor();
                $scope.$root.timers.monitor = $interval(refreshMonitor, 2000);
                $scope.$root.intflag = true;
            }
                // socketio.on('/api/sysmon', (data) => {
                //         // alert(data);
                //         data = JSON.stringify(data);
                //         alert(data);
                //         $scope.monitor.cpuLoad = data.result['DeviceCPU'];
                //         $scope.monitor.memoryUsage = data.result['DeviceMemory'];
                //         $scope.monitor.signalLevel = data.result['DeviceCommunications']
                //     });
            $scope.$root.greenBorderFlag = true;
            $scope.$root.version = `v${config.version}`;
            $scope.$root.title = config.deviceName;

        }, function (error) {
            //alert('error'+error);
        });
    }
]);
app.controller("MainPageCtrl", ['$scope', 'PageInit',
    function ($scope, PageInit) {

        $scope.$root.locationFlag = locationFlag = 'main';
        let dataPathByConfig = {
            'mock': {
                'local': window.localStorage.getItem('releAppMain'),
                'remote': 'api/main'
            },
            'production': {
                'local': null,
                'remote': ''
            }
        };
        PageInit(dataPathByConfig, $scope, locationFlag);
        $scope.test = 'Angular Works';
    }
]);
app.controller("SocketsCtrl", ['$scope', 'PageInit',
    function ($scope, PageInit) {

        $scope.$root.locationFlag = locationFlag = 'sockets';
        let dataPathByConfig = {
            'mock': {
                'local': window.localStorage.getItem('releAppSockets'),
                //'remote': "assets/js/mock/socketsPageData.json"
                'remote': 'api/sockets'
            },
            'production': {
                'local': null,
                'remote': ''
            }
        };
        PageInit(dataPathByConfig, $scope, locationFlag);
    }
]);
app.controller("PoeCtrl", ['$scope', 'PageInit',
    function ($scope, PageInit) {

        $scope.$root.locationFlag = locationFlag = 'poe';
        let dataPathByConfig = {
            'mock': {
                'local': window.localStorage.getItem('releAppPoe'),
                //'remote': "assets/js/mock/poePageData.json"
                'remote': 'api/poe'
            },
            'production': {
                'local': null,
                'remote': ''
            }
        };
        PageInit(dataPathByConfig, $scope, locationFlag);
    }
]);
app.controller("PirepheralCtrl", ['$scope', 'PageInit',
    function ($scope, PageInit) {

        $scope.$root.locationFlag = locationFlag = 'pirepheral';
        let dataPathByConfig = {
            'mock': {
                'local': window.localStorage.getItem('releAppPirepheral'),
                'remote': "assets/js/mock/pirepheralPageData.json"
            },
            'production': {
                'local': null,
                'remote': ''
            }
        };
        PageInit(dataPathByConfig, $scope, locationFlag);
    }
]);
app.controller("ServicesCtrl", ['$scope', 'PageInit',
    function ($scope, PageInit) {

        $scope.$root.locationFlag = locationFlag = 'services';
        let dataPathByConfig = {
            'mock': {
                'local': window.localStorage.getItem('releAppServices'),
                //'remote': "assets/js/mock/servicesPageData.json"
                'remote': 'api/services'
            },
            'production': {
                'local': null,
                'remote': ''
            }
        };
        PageInit(dataPathByConfig, $scope, locationFlag);
    }
]);
app.controller("CommunicationsCtrl", ['$scope', 'GetData',
    function ($scope, GetData) {
        $scope.$root.locationFlag = 'communications';
        /*    let url = "assets/js/mock/poePageData.json";
            GetData.get({
                url: url
            }, function (data) {
                $scope.poeObj = data;
            }, function (error) {
                //alert('error'+error);
            });*/
    }
]);
app.controller("SettingsCtrl", ['$scope', 'PageInit',
    function ($scope, PageInit) {

        $scope.$root.locationFlag = locationFlag = 'settings';
        let dataPathByConfig = {
            'mock': {
                'local': window.localStorage.getItem('releAppSettings'),
                //'remote': "assets/js/mock/settingsPageData.json"
                'remote': 'api/settings'
            },
            'production': {
                'local': null,
                'remote': ''
            }
        };
        PageInit(dataPathByConfig, $scope, locationFlag);
    }
]);
app.controller("StatCtrl", ['$scope',
    function ($scope) {
        $scope.$root.locationFlag = 'stats';
    }
]);
app.controller("WebproxyCtrl", ['$scope',
    function ($scope) {
        $scope.$root.locationFlag = 'webproxy';
    }
]);
app.controller("VpnCtrl", ['$scope',
    function ($scope) {
        $scope.$root.locationFlag = 'vpn';
    }
]);
app.controller("UpsCtrl", ['$scope',
    function ($scope) {
        $scope.$root.locationFlag = 'ups';
    }
]);
app.controller("LogCtrl", ['$scope',
    function ($scope) {
        $scope.$root.locationFlag = 'log';
    }
]);
app.controller("AuthCtrl", ['$scope', 'auth', 'config',
    function ($scope, auth, config) {
        $scope.$root.greenBorderFlag = false;
        $scope.$root.version = `v${config.version}`;
        $scope.message = "Choose your destiny";
        $scope.loginMethod = auth($scope).tryAuth;
        $scope.$root.title = `${config.deviceName} login page`;
        // alert('contr');
    }
]);